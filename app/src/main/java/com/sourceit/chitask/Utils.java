package com.sourceit.chitask;

import android.os.Environment;


public final class Utils {


    public static final String SP_NAME = "sp_name";
    public static final String SP_ON_CREATE = "sp_on_create";

    private Utils(){

    }

    public static boolean isExternalStorageWriteable(){
        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state)){
            return true;
        }
        return false;
    }
}
