package com.sourceit.chitask;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sourceit.chitask.Utils.SP_NAME;
import static com.sourceit.chitask.Utils.SP_ON_CREATE;

public class MainActivity extends AppCompatActivity {

    private static final String DIR_NAME = "testDir";
    private static final String FILE_NAME = "file_name";
    @BindView(R.id.sp_on_create)
    TextView onCreateSP;
    @BindView(R.id.sp_button)
    Button setZero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        SharedPreferences sp = getSharedPreferences(SP_NAME, MODE_PRIVATE);

        if(sp.getString(SP_ON_CREATE, "").isEmpty()){
            sp.edit()
                    .putString(SP_ON_CREATE, "1")
                    .apply();
            onCreateSP.setText(sp.getString(SP_ON_CREATE, "0"));
        }else {
            sp.edit()
                    .putString(SP_ON_CREATE, (Integer.parseInt(sp.getString(SP_ON_CREATE, "0")) + 1) + "")
                    .apply();
            onCreateSP.setText(sp.getString(SP_ON_CREATE, "0"));
        }


    }

    @OnClick (R.id.sp_button)
    void onSaveClick() {

        SharedPreferences sp = getSharedPreferences(SP_NAME, MODE_PRIVATE);
        sp.edit()
                .putString(SP_ON_CREATE, "0")
                .apply();
        onCreateSP.setText("0");
    }
}
